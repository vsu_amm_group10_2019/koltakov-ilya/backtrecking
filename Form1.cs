﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominoes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Run_Click(object sender, EventArgs e)
        {
            List<Dominoshka> many_domino = new List<Dominoshka>();
            //Создание объекта для генерации чисел
            Random rnd = new Random();
            for (int i = 0; i < numericDomino.Value; i++)
            {
                int left = rnd.Next(1, 6);
                int right = rnd.Next(1, 6);
                Dominoshka dominoshka = new Dominoshka(left, right);
                many_domino.Add(dominoshka);
            }
            Solver solver = new Solver(many_domino);
            MessageBox.Show(many_domino[0].left+"|"+many_domino[0].right+"\n"+
                            many_domino[1].left + "|" + many_domino[1].right + "\n" +
                            many_domino[2].left + "|" + many_domino[2].right + "\n" +
                "\nОтвет"+solver.Process(1).ToString());           
        }
    }
}
