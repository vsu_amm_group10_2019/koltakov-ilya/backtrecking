﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominoes
{
    public class Dominoshka
    {
        public int left { get; set; }
        public int right { get; set; }

        public Dominoshka(int left, int right)
        {
            this.left = left;
            this.right = right;
        }
    }
}
