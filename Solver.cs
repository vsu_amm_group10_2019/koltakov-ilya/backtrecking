﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominoes
{
    public class Solver
    {
        public List<Dominoshka> Many_Domino;

        public Solver(List<Dominoshka> many_domino)
        {
            Many_Domino = many_domino;
        }

        private bool Check(Dominoshka one, Dominoshka two, int left, int right)
        {
            if (one.left == two.left || one.right == two.right || one.right == two.left || one.right == two.right)
            {
                return true;
            }
            return false;

        }

        public int Process(int count)
        {
            int max = 0;
            for (int i = 0; i < Many_Domino.Count; i++)
            {
                
                bool[] visited = new bool[Many_Domino.Count];
                Dominoshka dominoshka = Many_Domino[i];

                int left = dominoshka.left;
                int right = dominoshka.right;
                int z = 0;
                if (Rim(dominoshka,visited, left, right, count,z) > max)
                    max = Rim(dominoshka,visited, left, right,count,z);
            }
            return max;
        }

        private int Rim(Dominoshka dominoshka,bool [] visited, int left, int right, int count, int i)
        {
            if (count == Many_Domino.Count || i==Many_Domino.Count)
                return count;

            Dominoshka compare_dominoshka = Many_Domino[i];

            if (dominoshka.Equals(compare_dominoshka))
            {
                i++;
                return Rim(dominoshka, visited, left, right, count, i);
            }
            else
            {
                if (left == compare_dominoshka.left)
                {
                    left = compare_dominoshka.right;
                    count++;
                    visited[i] = true;
                    i++;
                    return Rim(dominoshka, visited, left, right, count, i);
                }
                else if (left == compare_dominoshka.right)
                {
                    left = compare_dominoshka.left;
                    count++;
                    visited[i] = true;
                    i++;
                    return Rim(dominoshka, visited, left, right, count, i);
                }
                else if (right == compare_dominoshka.left)
                {
                    right = compare_dominoshka.right;
                    count++;
                    visited[i] = true;
                    i++;
                    return Rim(dominoshka, visited, left, right, count, i);
                }
                else if (right == compare_dominoshka.right)
                {
                    right = compare_dominoshka.left;
                    count++;
                    visited[i] = true;
                    i++;
                    return Rim(dominoshka, visited, left, right, count, i);
                }
                i++;
                return Rim(dominoshka, visited, left, right, count, i);
            }
        }
    }

}

